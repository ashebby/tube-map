import React, {Component} from "react";
import './SearchContainer.css';
import SearchInput from '../../components/Search/SearchInput/SearchInput';
import SearchOption from '../../components/Search/SearchOption/SearchOption';

class SearchContainer extends Component { 

    constructor(props) {
        super(props);
        this.state = {
            userInputDeparture: '',
            userInputArrival: '',
            depatureStationSelected: false,
            arrivalStationSelected: false,
            stations: this.props.data
        }
    }

    userInputDepatureHandler(event) {
        this.setState({
            userInputDeparture: event.target.value,
            depatureStationSelected: false
        })
    }

    departureStationSelectedHandler(event) {
        if(this.state.userInputDeparture.length > 0) {
            this.setState({
                userInputDeparture: event.target.value,
                depatureStationSelected: true
            })
        } 
    }

    userInputArrivalHandler(event) {
        this.setState({
            userInputArrival: event.target.value,
            arrivalStationSelected: false
        })
    }

    arrivalStationSelectedHandler(event) {
        if(this.state.userInputArrival.length > 0) {
            this.setState({
                userInputArrival: event.target.value,
                arrivalStationSelected: true
            })
        } 
    }

    render() {

        let filteredDepartureStations = this.state.stations.filter(
            (station) => {
                return (station.label.toLowerCase().indexOf(this.state.userInputDeparture.toLowerCase()) !== -1);
            }
        );

        let filteredArrivalStations = this.state.stations.filter(
            (station) => {
                return (station.label.toLowerCase().indexOf(this.state.userInputArrival.toLowerCase()) !== -1);
            }
        );

        return (
            <div>
                <div className="searchContainer">
                    <h1>PICK A DESTINATION</h1>

                    <div className="inputContainer">
                        <SearchInput
                            inputLabel="from:"
                            placeholder="departure"
                            inputValue={this.state.userInputDeparture}
                            userInputChangeEvent={this.userInputDepatureHandler.bind(this)}
                        />
                        {
                            filteredDepartureStations.map((station) => {
                                if((this.state.userInputDeparture.length >= 3) & !this.state.depatureStationSelected) {
                                    return(
                                        <SearchOption
                                            key={station.value}
                                            optionOnClickEvent={this.departureStationSelectedHandler.bind(this)}
                                            optionValue={station.label}
                                        />
                                    );
                                } 
                                return '';
                            })
                        }
                    </div>
                    <div className="inputContainer">
                        <SearchInput
                            inputLabel="to:"
                            placeholder="arrival"
                            inputValue={this.state.userInputArrival}
                            userInputChangeEvent={this.userInputArrivalHandler.bind(this)}
                        />
                        {
                            filteredArrivalStations.map((station) => {
                                if((this.state.userInputArrival.length >= 3) & !this.state.arrivalStationSelected) {
                                    return(
                                        <SearchOption
                                            key={station.value}
                                            optionOnClickEvent={this.arrivalStationSelectedHandler.bind(this)}
                                            optionValue={station.label}
                                        />
                                    );
                                } 
                                return '';
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchContainer;