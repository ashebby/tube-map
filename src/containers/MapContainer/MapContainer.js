import React, {Component} from "react";
import './MapContainer.css';
import Central from '../../components/TubeLines/CentralLine';

class MapContainer extends Component { 
    render() {
        return (
                <div className="mapContainer">
                    <Central />
                </div>
        );
    }
}

export default MapContainer;