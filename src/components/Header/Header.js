import React from 'react';
import './Header.css';

const Header = (props) => (
    <div className="headerContainer">
        <p className="title">TUBE MAP</p>
    </div>
);

export default Header;