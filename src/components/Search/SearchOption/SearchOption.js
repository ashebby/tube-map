import React from 'react';
import './SearchOption.css';

const SearchOption = (props) => (
    <div className="optionContainer">
        <option className="option" onClick={props.optionOnClickEvent}>
            {props.optionValue}
        </option>
    </div>
);

export default SearchOption;