import React from 'react';
import './SearchInput.css';

const SearchInput = (props) => (
    <div>
        {props.children}
        <label>{props.inputLabel}</label>
        <input 
            placeholder={props.placeholder}
            value={props.inputValue}
            onChange={props.userInputChangeEvent}
        />
    </div>
);

export default SearchInput;