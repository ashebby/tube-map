import React from "react"

const CentralLine = (props) => (
  <svg id='central' xmlns='http://www.w3.org/2000/svg' width="100%" height="100%" viewBox='0 0 3000 1900'>
    <path 
      fill='none' 
      stroke='#ED3024' 
      strokeWidth='6.651' 
      strokeLinejoin='round'
      strokeMiterlimit='3.994' 
      d='M517.761,819.79	v222.43c0,4.869,2.819,11.679,6.264,15.121l257.258,257.252c3.445,3.451,10.25,6.263,15.113,6.263 M2329.1,923.446	c0-7.607,6.16-13.771,13.771-13.771h127.069c7.601,0,13.771-6.166,13.771-13.776V740.993c0-7.604-6.17-13.775-13.771-13.775H2342.87	c-7.61,0-13.771,6.171-13.771,13.775 M2329.1,541.348v516.811c0,2.813-2.819,9.627-6.26,13.065l-147.74,147.741	c-3.439,3.445-10.239,6.257-15.109,6.257'
    />
    <path 
      fill='none' 
      stroke='#ED3024' 
      strokeWidth='6.65' 
      strokeLinejoin='round'
      strokeMiterlimit='3.994' 
      d='M2159.98,1225.223	H1959.97c-4.87,0-11.67,2.823-15.12,6.269l-128.859,128.873c-3.45,3.44-10.25,6.258-15.12,6.258h-79.49	c-4.87,0-11.66-2.817-15.11-6.258l-33.239-33.236c-3.44-3.448-10.24-6.264-15.11-6.264H613.041'
    />

    <circle id="EPP" cx='2329.27' cy='541.587' fill='#ED3024' r='10'/>
    <circle id="WRP" cx='517.08' cy='704.805' fill='#ED3024' r='10'/>


    {/* <rect x='2321.27' y='564.921' fill='#ED3024' width='7.841' height='4.386'
    />
    <rect x='2321.27' y='592.737' fill='#ED3024' width='7.841' height='4.385'
    />
    <rect x='2321.27' y='620.554' fill='#ED3024' width='7.841' height='4.388'
    />
    <rect x='2321.27' y='658.655' fill='#ED3024' width='7.841' height='4.387'
    />
    <rect x='2321.27' y='966.756' fill='#ED3024' width='7.841' height='4.393'
    />
    <rect x='2321.27' y='1048.471' fill='#ED3024' width='7.841' height='4.387'
    />
    <rect x='2321.26' y='810.759' fill='#ED3024' width='7.84' height='4.388'
    />
    <rect x='2329.833' y='779.831' fill='#ED3024' width='7.84' height='4.388'
    />
    <rect x='2321.26' y='843.716' fill='#ED3024' width='7.84' height='4.388'
    />
    <rect x='2475.86' y='797.45' fill='#ED3024' width='7.85' height='4.391'
    />
    <rect x='2483.737' y='763.907' fill='#ED3024' width='7.85' height='4.391'
    />
    <rect x='2475.86' y='830.413' fill='#ED3024' width='7.85' height='4.391'
    />
    <rect x='2475.86' y='863.374' fill='#ED3024' width='7.85' height='4.387'
    />
    <rect x='509.921' y='834.893' fill='#ED3024' width='7.84' height='4.391'
    />
    <rect x='510.5' y='892.109' fill='#ED3024' width='7.84' height='4.391'
    />
    <rect x='509.921' y='970.1' fill='#ED3024' width='7.84' height='4.388'
    />
    <polyline fill='#ED3024' points='583.556,1127.958 580.457,1124.855 585.998,1119.314 589.103,1122.417 583.556,1127.958'
    />
    <polyline fill='#ED3024' points='533.556,1079.291 530.457,1076.188 535.998,1070.647 539.103,1073.75 533.556,1079.291'
    />
    <polyline fill='#ED3024' points='638.826,1183.229 635.718,1180.124 641.267,1174.579 644.371,1177.68 638.826,1183.229'
    />

    
    <rect x='735.697' y='1320.874' fill='#ED3024' width='4.385' height='7.84'
    />
    <rect x='613.697' y='1322.874' fill='#ED3024' width='4.385' height='7.84'
    />
    <rect x='837.476' y='1320.874' fill='#ED3024' width='4.391' height='7.84'
    />
    <rect x='2404.02' y='727.204' fill='#ED3024' width='4.391' height='7.84'
    />
    <rect x='2445.26' y='909.657' fill='#ED3024' width='4.391' height='7.84'
    />


    <rect x='2366.93' y='909.657' fill='#ED3024' width='4.391' height='7.84'
    />
    <rect x='2406.09' y='901.835' fill='#ED3024' width='4.391' height='7.836'
    />
    <rect x='2442.16' y='719.38' fill='#ED3024' width='4.39' height='7.838'
    />
    <rect x='2364.981' y='718.161' fill='#ED3024' width='4.39' height='7.838'
    />
    <rect x='2074.14' y='1217.352' fill='#ED3024' width='4.391' height='7.84'
    />
    <rect x='2141.172' y='1215.738' fill='#ED3024' width='4.391' height='7.84'
    />
    <rect x='1882.806' y='1280.017' transform='rotate(135.008 1885.01 1283.96)'
    fill='#ED3024' width='4.391' height='7.84' />


    <polyline fill='#ED3024' points='1686.15,1340.227 1683.04,1337.126 1688.59,1331.575 1691.689,1334.68 1686.15,1340.227'
    />
    <rect x='1064.38' y='1320.874' fill='#ED3024' width='4.397' height='7.84'
    />
    <rect x='1006.38' y='1321.541' fill='#ED3024' width='4.397' height='7.84'
    />
    <rect x='1178.283' y='1320.874' fill='#ED3024' width='4.39' height='7.84'
    />
    <rect x='1265.77' y='1320.874' fill='#ED3024' width='4.39' height='7.84'
    />
    <rect x='869.869' y='1313.15' fill='#ED3024' width='4.425' height='7.744'
    />
    <rect x='941.409' y='1312.5' fill='#ED3024' width='4.425' height='7.744'
    />
    <rect x='1649.53' y='1313.15' fill='#ED3024' width='4.43' height='7.744'
    />
    <rect x='1225.54' y='1313.15' fill='#ED3024' width='4.42' height='7.744'
    />
    <rect x='1309.54' y='1310.483' fill='#ED3024' width='4.42' height='7.744'
    />
    <rect x='1394.207' y='1321.816' fill='#ED3024' width='4.42' height='7.744'
    />
    <rect x='1512.873' y='1322.482' fill='#ED3024' width='4.42' height='7.744'
    />
    <rect x='1574.873' y='1311.149' fill='#ED3024' width='4.42' height='7.744'
    />
    <rect x='1120.747' y='1313.166' fill='#ED3024' width='4.42' height='7.744'
    /> */}

    <line 
      fill='none' stroke='#ED3024' strokeWidth='6.651' strokeLinejoin='round'
      strokeMiterlimit='3.994' x1='517.761' y1='704.651' x2='517.761' y2='821.618'
    />
</svg>
)

export default CentralLine;
